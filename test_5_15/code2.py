#面向对象编程（OOP）
#核心概念：类，对象
#什么是类
#是一类事物抽象的概念，描绘了一类事物共同的事物和特征
#类定义的语法：
#植物大战僵尸的类
#sunflower
class sunflower:
    pass
class pea:
    pass
class cat:
    pass
#类的属性和方法
#类属性（共有属性（直接在类里面的），私有属性（以双下划线开头的变量））
#通过类可以直接访问共有属性，二在类外部私有属性不能访问
class man:
    leg=2
    arm=4
    __eye=2
    def skill(self):
        print('会爬树')
    #类方法的定义
    @classmethod#装饰
    def print_eye(cls):
        print(cls.__eye)
    #类方法的定义需要使用@classmethod去进行声明
    #类方法中必须定义一个参数（在调用时，用来接收类），参数明通常为cls
    #可以通过cls去访问私有属性
    #静态方法的定义
    @staticmethod
    def print_eye(self):
        print(cat.__eye)
    #静态方法的定义时，必须使用@staticmethod来进行声名
    #一般情况用在类里面去封装一些和当前类无关的功能函数（方法的功能实现，不会使用类的属性和方法）

print(man.leg)
# print(man.__leg)
#通过类名.属性名获取类对应的属性值
#类里面定义的方法：定义在类里面的函数叫做方法
#1、类方法2、静态方法3、对象方法


#一、对象
#每一个数据类型实际上都是一个类
#对象是通过类创建出来的，对象可以访问所有的属性和方法
class cat:
    leg=4
    eye=2
    tail=1
    __id='金莎'
    @classmethod
    def print_id(cls):
        print(cls.__id)
    @staticmethod
    def hehe():
        print('杨晓英喜欢金莎')
#对象可以访问类里面的所有属性
dd=cat()
print(dd.leg,dd.eye,dd.tail)
dd.hehe()
dd.print_id()

print('3333333333333333333333')
#二、对象方法（实例方法）
#在实际的使用中类里面90%的方法会被定义成对象方法
class phone:
    def send_message(self):
        print('会发送信息')
    def call(self):
        print('开始打电话')
    @classmethod
    def haha(cls):
        print('haha')
    @staticmethod
    def xixi():
        print('xixi')
mate60=phone()
iphone=phone()
mate60.call()#此时的self即是mate60
iphone.call()#此时的self即是iphone
#类：特性（属性）：数据；行为（方法）：动作
#对象属性往往是类里面的函数,第一个参数是self
#self参数代表的是当前对象本身（哪个对象调用的方法，self就是哪个对象）
#对象方法的访问
#类方法：类和对象都能直接调用
#静态方法：类和对象都能直接调用
#对象方法：稚嫩恶搞通过对象去调用，类无法直接调用
