#字典的创建
#键值对#键不能重复
a={}
b=dict()
print(type(a))
print(type(b))
a={'id':1,'name':'zhangsan'}
print(a)
#最后一个键值对后面的，可有可无
#查找key
#使用in来判断某个key是否在字典中存在，只能判断key，而不能判断value
student={
    'age':12,
    'name':'yxy'
}
print('age' in student)
print('sb' in student)
#not in 判断键在字典中不存在
print('yxy' not in a)
#使用[]来根据key得到value
student={
    'age':12,
    'name':'yxy',
    100:'haha'
}
print(student['age'])
print(student[100])

#字典的in操作使用了特殊的数据结构：哈希表
#字典的新增和修改操作
a={
    'age':12,
    'name':'yxy',
    100:'haha'
}
a['age']=24
a['id']='xixi'
print(a)
#类似于变量的修改和新增
#键值对的删除 a.pop()
b={
    'age':12,
    'name':'yxy',
    100:'haha'
}
b.pop('name')
print(b)
#字典的各种操作都是对键操作的
#字典的遍历操作
#字典实际上是一个哈希表，进行增删的操作效率都是非常高的
#哈希表能够以“常数级”时间复杂度来完成增删操作
#无论字典中有多少元素，增删改查都是固定时间
#字典的遍历
#直接使用for循环
b={
    'age':12,
    'name':'yxy',
    100:'haha'
}
# for key in b:
#     print(key,b[key])
#还可使用keys获取得到字典中所有的key，values后去得到字典中所有的value
#items获取得到字典中的所有键值对
print(b.keys())
print(b.values())
print(b.items())
for key,value in b.items():
    print(key,value)
#字典合法key的类型
#哈希函数
print(hash((1,2,3)))
# print(hash([1,2,3]))
