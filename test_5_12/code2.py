#python函数
#哈希函数
def cal_hash(key):
    square=ord(key)**2
    mid=str(square)[3:6]#取的值是平方书中的三位数字作为返回值
    return int(mid)
print(cal_hash("瓜"))
#ord(c)[]#它以一个字符（长度为1的字符串）作为参数，返回对应的 ASCII 数值
#print()
print('hello world',end=' ')
print('hello world','kuku',1234,sep='www',end='\n')
print('haha\n')
print('haha',end='\n')
print('haha\n',end='')
#在打印的末尾加sep='',控制打印的中间用什么隔开
#在末尾打印end='',控制是否有换行操作
#file:输出位置，技术处到文件还是命令行,默认为sys.student,即命令行终端
#索引字符：%e是代表以科学计数法输出
age=18
name='xiaohua'
print("我是%s,今年%d岁"%(name,age))
print(f"我是{name}，今年{age}岁")
#input()
print(int(input('请输入用户名')))
# print(int(input(请输入用户名)))#错误，字符串需要用引号引起来
#str()将对象转化为适于人阅读的形式
s = 'RUNOOB'
print(str(s))
'RUNOOB'#代表注释
dict = {'runoob': 'runoob.com', 'google': 'google.com'};
str(dict)
"{'google': 'google.com', 'runoob': 'runoob.com'}"
'hha'


