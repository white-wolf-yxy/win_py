#创建列表
#[]就表示一个空的列表
a=[]
print(type(a))
#使用list()来创建
b=list()
print(type(b))
#可以在创建列表的时候，在[]中指定列表的初始值
#可以在列表中放不同类型的变量
a=[1,True,'hello',[4,5,6]]
print(a)
b=list([1,'hello',True,[2,4,8]])#标记
print(b)
a=[1,2,1,2]
print(a)
#使用下标访问
c=[1,2,3,4,5,6,7]
print(c[0])
print(c[-1])
#使用下标修改列表元素
c[0]=90
print(c)
print(a[len(a)-1])
print(a[-1])
#切片操作
print(a[2:3])#返回值是一个列表
print(a[1:])
print(a[:])
#切片操作可以设置步长在原有的基础上再加一个：和步长大小#标记
#步长可以是负数，从后往前去取数
b=[1,2,3,4,5,6,7,8,9,10]
print(b[::2])
print(b[::1])
print(b[1:-1:2])
print(b[::-2])
#列表元素的遍历#把一个列表里面的每个元素，都依次取出来，并进行某种操作#搭配循环#标记
#使用for循环来遍历列表
xixi=[1,2,3,4,5,6,7,8,9]
for elem in xixi:
    print(elem)#返回元素是一个值，而非列表
    #for in需要是一个可迭代对象，左闭右开
a=[1,2,3,4,5]
for i in range(0,len(a)):
    print(a[i])
#新增元素
a.append(5)
a.append(['hheh'])
print(a)
#append()是一个方法（method）
#insert方法，往列表的任意位置来新增元素
a=[1,2,3,4,5]
a.insert(1,'hello')
a.insert(1000,'hello')
print(a)
#使用in判断某个元素是否存在其中
b=[2,3,4,5]
print(2 in b)
print(6 not in b)
#使用index方法，来判定当前元素在列表中的位置，得到一个下标
a=[1,2,34,5]
print(a.index(2))
print(a)
# print(a.index(10))#直接报异常，不会返回-1（像c++,java一样）
#是用pop删除列表中的最末尾的元素
a=[1,2,3,4]
a.pop()
print(a)
#使用pop还可以删除任意位置的元素，pop的参数可以穿一个下标过去，把对应位置的元素删掉
a.pop(2)
print(a)
#使用remove方法，可以按照值来删除
a=['hehe',2,3,4,5]
print(a)
#列表的拼接
#使用相加拼接#需要额外赋值新的变量
a=[1,2,3,4]
b=[5,6,7,8]
print(a+b)
#使用extend来进行拼接#不需要额外赋值新的变量
a=[1,2,3,4]
b=[5,6,7,8]
a.extend
print(a)
print(b)
#还可以利用+=来进行拼接#不需要额外赋值新的变量
a=[1,2,3,4]
b=[5,6,7,8]
a+=b
print(a)
# a=[1,2,3,4]
# a.append('hehe')
# print(a)
# print(type(a.append(['sb'])))#尽管在print里面仍然会执行括号里面的操作
# print(a)

# a=[1,2,3,4]
# b=[5,6,7,8]
# print(type(a.extend(b)))
# print(a)
# print(b)#也会执行print括号里面的操作
# a=[1,2,3,4]
# print(type(a.pop()))
# print(a)







