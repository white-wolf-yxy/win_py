#列表
#append()和pop()的区别
queue = []
import copy
# # 添加元素到队列的末尾
# queue.append('A')
# queue.append('B')
# queue.append('C')
# print(queue)
# # 从队列的开头删除元素并返回
# print(queue.pop(0))  # A
# print(queue.pop(0))  # B
# print(queue.pop(0))  # C
#append()和extend()的区别
def changeextend(str):
    "print string with extend"
    mylist.extend([40,50,60]);
    print ("print string mylist:",mylist)
    return
def changeappend(str):
    "print string with append"
    mylist.append( [7,8,9] )
    print("print string mylist:",mylist )
    return
mylist = [10,20,30]
changeextend( mylist );
print ("print extend mylist:", mylist )
changeappend( mylist );
print ("print append mylist:", mylist )
#append() 方法向列表的尾部添加一个新的元素。
#列表是以类的形式实现的。“创建”列表实际上是将一个类实例化。因此，列表有多种方法可以操作。extend()方法只接受一个列表作为参数，并将该参数的每个元素都添加到原有的列表中
alist = []
num = [2]
alist.append( num )
# id( num ) == id( alist[0] )
print(alist)
num[0]=3
print(alist)
alist = []
num = [2]
alist.append(copy.deepcopy(num))
# id( num ) == id( alist[0] )
print(alist)
num[0]=3
print(alist)
#append() 是浅拷贝,如果使用 num[0]=3，改变 num 后，alist[0] 也随之改变。

#如不希望，需要使用 alist.append( copy.deepcopy( num ) )

#可以使用 del 语句来删除列表的的元素，如下实例
list = ['Google', 'Runoob', 1997, 2000]

print("原始列表 : ", list)
del list[2]
print("删除第三个元素 : ", list)
#列表对 + 和 * 的操作符与字符串相似。+ 号用于组合列表，* 号用于重复列表。
test=[1,2,3]
print(type(test))
test=[1]
print(type(test))
test=[1,2,3]
test2=[4,5,6]
print(len(test))
print(test+test2)
print(test*3)
#元素是否存在列表中
print(3 in test)
#迭代
for x in [1, 2, 3]:
    print(x, end=" ")
#列表还支持拼接操作：
print(test+[4,3,5])
#嵌套列表
x=[test,test2]
print(x)
print(x[0])
print(x[0][1])
#列表的比较
#列表比较需要引入 operator 模块的 eq 方法（详见：Python operator 模块）：
import operator
a = [1, 2]
b = [2, 3]
c = [2, 3]
print("operator.eq(a,b): ", operator.eq(a,b))
print("operator.eq(c,b): ", operator.eq(c,b))
#operator 模块
#数
x=10
y=20
print("x=",x,"y=",y)
print("operator.lt(x,y): ", operator.lt(x,y))
print("operator.gt(y,x): ", operator.gt(y,x))
print("operator.eq(x,x): ", operator.eq(x,x))
print("operator.ne(y,y): ", operator.ne(y,y))
print("operator.le(x,y): ", operator.le(x,y))
print("operator.ge(y,x): ", operator.ge(y,x))
print()
# 字符串
x = "Google"
y = "Runoob"
print("x:",x, ", y:",y)
print("operator.lt(x,y): ", operator.lt(x,y))
print("operator.gt(y,x): ", operator.gt(y,x))
print("operator.eq(x,x): ", operator.eq(x,x))
print("operator.ne(y,y): ", operator.ne(y,y))
print("operator.le(x,y): ", operator.le(x,y))
print("operator.ge(y,x): ", operator.ge(y,x))
print()
# 查看返回值
print("type((operator.lt(x,y)): ", type(operator.lt(x,y)))
#比较两个列表
a = [1, 2]
b = [2, 3]
c = [2, 3]
print("operator.eq(a,b): ", operator.eq(a,b))
print("operator.eq(c,b): ", operator.eq(c,b))
#字符串的比较
string3 = "apple"
string4 = "banana"

# 使用比较运算符（字典顺序）
if string3 < string4:
    print(f"{string3} 在 {string4} 前面")
else:
    print(f"{string3} 在 {string4} 后面")
string_a = "Python"
string_b = "is fun"

# 比较字符串长度
if len(string_a) == len(string_b):
    print("这两个字符串长度相同")
else:
    print("这两个字符串长度不同")
#运算符函数
# add(), sub(), mul()

# 导入  operator 模块
import operator

# 初始化变量
a = 4
b = 3
# 使用 add() 让两个值相加
print("add() 运算结果 :", end="");
print(operator.add(a, b))

# 使用 sub() 让两个值相减
print("sub() 运算结果 :", end="");
print(operator.sub(a, b))

# 使用 mul() 让两个值相乘
print("mul() 运算结果 :", end="");
print(operator.mul(a, b))
