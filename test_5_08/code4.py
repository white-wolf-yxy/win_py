#字典
#字典是另一种可变容器模型，且可存储任意类型对象。
#字典的每个键值 key=>value 对用冒号 : 分割，每个对之间用逗号(,)分割，整个字典包括在花括号 {} 中 ,格式如下所示：
tinydict = {'name': 'runoob', 'likes': 123, 'url': 'www.runoob.com'}
#dict 作为 Python 的关键字和内置函数，变量名不建议命名为 dict。
#键必须是唯一的，但值则不必。
#值可以取任何数据类型，但键必须是不可变的，如字符串，数字。
print(tinydict)
#创建空字典
emptydict={}
print(emptydict)
print(len(emptydict))
print(type(emptydict))
#用于内置函数dict()
numbers = dict(x=5, y=0)
print('numbers =', numbers)
print(type(numbers))
empty = dict()
print('empty =', empty)
print(type(empty))
#设置可迭代对象创建字典
# 没有设置关键字参数
numbers1 = dict([('x', 5), ('y', -5)])
print('numbers1 =',numbers1)

# 设置关键字参数
numbers2 = dict([('x', 5), ('y', -5)], z=8)
print('numbers2 =',numbers2)

# zip() 创建可迭代对象
#将两个列表合成表
numbers3 = dict(dict(zip(['x', 'y', 'z'], [1, 2, 3])))
print('numbers3 =',numbers3)
#访问字典里的值
#把相应的键放入到方括号中，如下实例:
tinydict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
print("tinydict['Name']: ", tinydict['Name'])
print("tinydict['Age']: ", tinydict['Age'])
#如果访问字典里没有的键，会输出错误
#修改字典（向字典添加新内容的方法是增加新的键/值对，修改或删除已有键/值对）
tinydict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}

tinydict['Age'] = 8  # 更新 Age
tinydict['School'] = "菜鸟教程"  # 添加信息

print("tinydict['Age']: ", tinydict['Age'])
print("tinydict['School']: ", tinydict['School'])
print(tinydict)
#能删单一的字典，也能删除一整行
tinydict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}

del tinydict['Name']  # 删除键 'Name'
# tinydict.clear()  # 清空字典
# del tinydict  # 删除字典

print("tinydict['Age']: ", tinydict['Age'])

print(tinydict)
#字典键的特性#字典值可以是任何的 python 对象，既可以是标准的对象，也可以是用户定义的，但键不行
#1）不允许同一个键出现两次。创建时如果同一个键被赋值两次，后一个值会被记住
tinydict = {'Name': 'Runoob', 'Age': 7, 'Name': '小菜鸟'}
print("tinydict['Name']: ", tinydict['Name'])
print(tinydict)
#2）键必须不可变，所以可以用数字，字符串或元组充当，而用列表就不行
# tinydict = {['Name']: 'Runoob', 'Age': 7}
#
# print("tinydict['Name']: ", tinydict['Name'])
#字典的内置函数
tinydict = {'Name': 'Runoob', 'Age': 7, 'Class': 'First'}
print(str(tinydict))
