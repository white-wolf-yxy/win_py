#集合
#集合（set）是一个无序的不重复元素序列。
#集合中的元素不会重复，并且可以进行交集、并集、差集等常见的集合操作。
set1 = {1, 2, 3, 4}            # 直接使用大括号创建集合
set2 = set([4, 5, 6, 7])      # 使用 set() 函数从列表创建集合
print(set1)
print(set2)
#注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。
# >>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
# >>> print(basket)                      # 这里演示的是去重功能
# {'orange', 'banana', 'pear', 'apple'}
# >>> 'orange' in basket                 # 快速判断元素是否在集合内
# True
# >>> 'crabgrass' in basket
# False
#
# >>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
# >>> print(basket)                      # 这里演示的是去重功能
# {'orange', 'banana', 'pear', 'apple'}
# >>> 'orange' in basket                 # 快速判断元素是否在集合内
# True
# >>> 'crabgrass' in basket
# False

# >>> # 下面展示两个集合间的运算.
# ...
# >>> a = set('abracadabra')
# >>> b = set('alacazam')
# >>> a
# {'a', 'r', 'b', 'c', 'd'}
# >>> a - b                              # 集合a中包含而集合b中不包含的元素
# {'r', 'd', 'b'}
# >>> a | b                              # 集合a或b中包含的所有元素
# {'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}
# >>> a & b                              # 集合a和b中都包含了的元素
# {'a', 'c'}
# >>> a ^ b                              # 不同时包含于a和b的元素
# {'r', 'd', 'b', 'm', 'z', 'l'}
#集合的基本操作
#添加元素
#将元素x添加到集合s中
#方法一
s={1,2,3,4}
s.add(5)#括号中只能添加一个
print(s)
#方法二
#可以添加元素，且参数可以是列表，元组，字典等
s.update({7,6})
print(s)