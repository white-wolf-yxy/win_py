#元组
#Python 的元组与列表类似，不同之处在于元组的元素不能修改。
#元组使用小括号 ( )，列表使用方括号 [ ]。
tup1 = ('Google', 'Runoob', 1997, 2000)
tup2 = (1, 2, 3, 4, 5 )
tup3 = "a", "b", "c", "d"   #  不需要括号也可以
print(type(tup3))
#元组中只包含一个元素时，需要在元素后面添加逗号 , ，否则括号会被当作运算符使用：
tup1 = (50)
type(tup1)     # 不加逗号，类型为整型
#<class 'int'>
print(tup1)

tup1 = (50,)
type(tup1)     # 加上逗号，类型为元组
#<class 'tuple'>
print(tup1)
#访问元组
# !/usr/bin/python3

tup1 = ('Google', 'Runoob', 1997, 2000)
tup2 = (1, 2, 3, 4, 5, 6, 7)

print("tup1[0]: ", tup1[0])#Google
print("tup2[1:5]: ", tup2[1:5])
#元组中的元素值是不允许修改的，但我们可以对元组进行连接组合，如下实例:
# !/usr/bin/python3

tup1 = (12, 34.56)
tup2 = ('abc', 'xyz')

# 以下修改元组元素操作是非法的。
# tup1[0] = 100

# 创建一个新的元组
tup3 = tup1 + tup2
print(tup3)
#元组中的元素值是不允许删除的，但我们可以使用del语句来删除整个元组，如下实例:

# tup = ('Google', 'Runoob', 1997, 2000)
# print(tup)
# del tup
# print("删除后的元组 tup : ")
# print(tup)
#元组内置函数
a=(1,2,3)
b=(4,5,6)
print(len(a))
print(max(a))
print(min(a))
#将可迭代序列转换为元组
list=[1,2,5]
tuple1=tuple(list)
print(tuple1)
#关于元组是不可变的
tup = ('r', 'u', 'n', 'o', 'o', 'b')
print(id(tup))     # 查看内存地址
tup = (1,2,3)
print(id(tup))# 内存地址不一样了
#从以上实例可以看出，重新赋值的元组 tup，绑定到新的对象了，不是修改了原来的对象。
