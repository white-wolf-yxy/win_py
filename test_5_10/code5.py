#集合
#集合（set）是一个无序的不重复元素序列。
#集合中的元素不会重复，并且可以进行交集、并集、差集等常见的集合操作。
set1 = {1, 2, 3, 4}            # 直接使用大括号创建集合
set2 = set([4, 5, 6, 7])      # 使用 set() 函数从列表创建集合
print(set1)
print(set2)
#注意：创建一个空集合必须用 set() 而不是 { }，因为 { } 是用来创建一个空字典。
# >>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
# >>> print(basket)                      # 这里演示的是去重功能
# {'orange', 'banana', 'pear', 'apple'}
# >>> 'orange' in basket                 # 快速判断元素是否在集合内
# True
# >>> 'crabgrass' in basket
# False
#
# >>> basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
# >>> print(basket)                      # 这里演示的是去重功能
# {'orange', 'banana', 'pear', 'apple'}
# >>> 'orange' in basket                 # 快速判断元素是否在集合内
# True
# >>> 'crabgrass' in basket
# False

# >>> # 下面展示两个集合间的运算.
# ...
# >>> a = set('abracadabra')
# >>> b = set('alacazam')
# >>> a
# {'a', 'r', 'b', 'c', 'd'}
# >>> a - b                              # 集合a中包含而集合b中不包含的元素
# {'r', 'd', 'b'}
# >>> a | b                              # 集合a或b中包含的所有元素
# {'a', 'c', 'r', 'd', 'b', 'm', 'z', 'l'}
# >>> a & b                              # 集合a和b中都包含了的元素
# {'a', 'c'}
# >>> a ^ b                              # 不同时包含于a和b的元素
# {'r', 'd', 'b', 'm', 'z', 'l'}
#集合的基本操作
#添加元素
#将元素x添加到集合s中
#方法一
s={1,2,3,4}
s.add(6)#括号中只能添加一个
print(s)
#方法二
#可以添加元素，且参数可以是列表，元组，字典等
s.update({7,6})
print(s)
s.update([2,3,4,5],[8,9])
print(s)
#移除元素
#将元素 x 从集合 s 中移除，如果元素不存在，则会发生错误。
thisset = set(("Google", "Runoob", "Taobao"))
thisset.remove("Taobao")
print(thisset)
#thisset.remove("Facebook")   # 不存在会发生错误
#还有一种方法，移除集合中的元素，且元素不存在，不会发生错误
thisset.discard("Facebook")
#我们也可以设置随机删除集合中的一个元素，语法格式如下：
# s.pop()
thisset = set(("Google", "Runoob", "Taobao", "Facebook"))
x = thisset.pop()
print(x)
print(thisset)
# 多次执行测试结果都不一样。
# set 集合的 pop 方法会对集合进行无序的排列，然后将这个无序排列集合的左面第一个元素进行删除。
#交集并集差集补集
x=set('eleven')
y=set('twelve')
#交集  包括同时在集合 x 和y中的共同元素。
print(x&y)
#并集  包括集合 x 和 y 中所有元素。
print(x|y)
#差集 包括在集合 前者 中但不在集合 后者 中的元素。
print(x-y)
print(x-y)
#补集 返回一个新的集合，包括集合 x 和 y 的非共同元素。
print(x^y)
#计算集合元素的个数
s=set((1,2,3,4))
print(len(s))
#清空集合
s1=set((1,23,4,44))
s1.clear()
print(s1)
#判断集合中是否存在
s2=((3,4,5,6,78))
print(3 in s2)
print(99 in s2)