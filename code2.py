#人生重开模拟器
#实现简化版本
#游戏开始的时候，设定初始属性（颜值，体质，智力，家境）
#开始游戏，随机生成行呗和出生点
#针对每一年，都成为人生的经历（依靠一定的随机因素+当前角色的属性）
#打印一个欢迎界面
import random
import sys
#pycharm自动导入当前使用的模块，
#在python中如果想引入其他模块，需要先用import语句，把模块的名字给导入进来
print("+------------------------------------+")
print("|         花有重开日，人无再少年         |")
print("|         欢迎来到人生重开模拟器         |")
print("+------------------------------------+")
#设置初始属性，颜值，体质，智力，家境（可用点数总数为20，每个在一到十之间）
#通过条件语句对用户的输入进行校验
#使用elif，只能执行一次
#方便用户输错更改程序，采取循环（while True）
while True:
    print("请输入初始属性颜值，体质，智力，家境（可用点数总数为20，每个在一到十之间）")
    face=int(input())
    strong=int(input())
    iq=int(input())
    familycirtuation=int(input())
    if face < 1 and face > 10:
        print('输入错误')
        continue
    if strong < 1 and strong > 10:
        print('输入错误')
        continue
    if iq<1 and iq>10:
        print('输入错误')
        continue
    if familycirtuation<1 and familycirtuation>10:
        print('输入错误')
        continue
    if strong+iq+familycirtuation+face>20:
        print('输入错误，请看好规定')
        continue

    print("属性输入完毕")
    print(f'颜值；{face},体质：{strong},智力：{iq}，家境：{familycirtuation}')
    break
#合法非法是指的是是否合理，是否有效
#随机生成角色的性别
#使用random.randint(beg,end),就能随机生成【beg，end】随机整数
point=random.randint(1,6)
if point % 2==0:
    print('这一世你为男生')
    gender='boy'
if point % 2!=0:
    print('这一世你为女生')
    gender='girl'
#设置角色的出生点（家境+随机数）
# 家境9第一档，带来一些属性加成
# 家境7-8第二档，也会带来一些属性加成
# 家境4-6第三档，少数属性加成
# 家境1-3第四档，会扣属性
point=random.randint(1,3)
if familycirtuation==9:
    print("你出生在帝都，你的父母是高官政要")
    familycirtuation+=1
    iq+=1
    face+=1
elif 7<=familycirtuation<=8:
    if point==1:
        print("你出生在大城市，父母是公务员")
        face+=2
    elif point==2:
        print("你出生在大城市，父母是企业高管")
        familycirtuation+=2
    else:
        print('你出生在大城市，父母是大学教授')
        iq+=2
elif 4<=familycirtuation<=6:
    if point==1:
        print("你出生在三线城市，父母是医生")
        strong+=1
    elif point==2:
        print("你出生在镇上，父母是老师")
        iq+=1
    else:
        print('你出生在镇上，父母是个体户')
        familycirtuation+= 1
elif 1<=familycirtuation<=3:
    if point==1:
        print("你出生在农村，父母是农民")
        strong+=1
        face-=2
    elif point==2:
        print("你出生在穷乡僻壤，父母是无业游民")
        familycirtuation-=1
    else:
        print('你出生在镇上，父母感情不和')
        strong -= 1
print(f'颜值；{face},体质：{strong},智力：{iq}，家境：{familycirtuation}')
#重要的是平衡性的设定
#生成角色的人生经历
#幼年阶段【1，10】可塑性强
#先使用一个循环，按照年龄，从1循环到10；针对每一年，都随机生成一个随机数【1，3】；根据角色，性别，年龄，各种属性，触发各种事件，随机数会对事件的结果造成影响；这里的事件可能对属性带来变更，每一年执行结束，都打印在这一年发生的事件（每年只允许发生一个事件）
#也可能发生夭折，使用exit函数来结束程序
for age in range(1,11):
    info=f'你今年{age}岁'
    point=random.randint(1,2)
    #由性别触发的事件
    if gender=='girl'and familycirtuation<=4 and point==1:
        info+='重男轻女，你被遗弃了'
        print(info)
        sys.exit(0)
        print('游戏结束')
    #体质触发的事件
    elif strong<6 and point<3:
        info+='你生了一场病'
        if familycirtuation>=5:
            info+='父母照顾，你康复了'
            print(info)
            strong+=1
            familycirtuation-=1
        else:
            info+='你父母选择放弃你'
            print(info)
            sys.exit(0)
            print('游戏结束')
    #颜值触发的事件
    elif face<=4 and age>=7:
        info+='容貌焦虑，发奋图强'
        print(info)
        iq+=1
        face+=1
    elif iq<5:
        info+='看起来傻'
        if familycirtuation>=8 and age>=6:
            info+='你父母把你送到更好的学校读书'
            iq+=1
        elif 4<=familycirtuation<=7:
            if gender=='boy':
                info+='你父母鼓励你多运动，争取成为运动员'
                strong+=1
            else:
                info+='你父母鼓励你多打扮'
                face+=1
        else:
            info+='你父母为此经常吵架'
            strong-=1
print(f'颜值；{face},体质：{strong},智力：{iq}，家境：{familycirtuation}')

#青年阶段【11，20】求学
#壮年阶段【21，50】平稳
#老年阶段 50岁以上 颜值和体质和智力显著退化
