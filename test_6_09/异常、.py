#针对性处理异常，抛出异常
#捕获异常
# num=int(input("请输入一个整数"))
try:
    num = int(input("请输入一个整数"))#无论是否出现错误，都会执行后序的代码
except:
    print("请输入正确的整数",end="")
    num=int(input())
print("-"*50)
