class Musicplayer:
    instance=None
    init_flag=False
    def __new__(cls, *args, **kwargs):
        if cls.instance is None:#判断类属性是否为空对象
            cls.instance=super().__new__(cls)#调用一个父类的方法，为第一个对象分配空间
        return cls.instance#返回类属性保存的对象引用
    def __init__(self):
        if Musicplayer.init_flag:
            return
        print("初始化防备执行")
        Musicplayer.init_flag=True
#创建多个对象
player1=Musicplayer()
print(player1)
player2=Musicplayer()
print(player2)

