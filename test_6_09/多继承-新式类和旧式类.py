# 以object为基类的类是新式类，反之为旧式（经典）类
# dir函数查看一些内置属性和方法
class A:
    pass
a=A()
print(dir(a))
class B(object):
    pass
b=B()
print(dir(b))
    