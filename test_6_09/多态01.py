#多态不同的子类对象调用相同的父类方法，产生不同的执行结果
#多态可以增加代码的灵活度，以继承和重写父类方法为前提，是调用方法的技巧，不会影响到类的内部设计
class Dog(object):
    def __init__(self,name):
        self.name=name
    def game(self):
        print("%s玩耍"%(self.name))
class Xiaotiandog(Dog):
    def game(self):
        print("%s在天上玩耍"%(self.name))
class Person(object):
    def __init__(self,pname):
        self.pname=pname
    def play_with_dog(self,dog):
        print("%s和%s玩"%(self.pname,dog.name))
        dog.game()
xiaotianquan=Xiaotiandog("哮天犬")
xiaoming=Person("小明")
xiaoming.play_with_dog(xiaotianquan)

