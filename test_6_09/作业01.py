#01
# 编程实现：判断用户输入账号、密码是否正确。
# 提示：预先设定一个账号：xiaoli，密码：123456，如果账号和密码都正确，
# 就显示“Hello，好久不见！”，否则，输出“对不起，账号或密码输入有误。”
# account=str(input("请输入账号"))
# key=int(input("请输入密码"))
# if key==123456 and account=="xiaoli":
#     print("Hello，好久不见！")
# else:
#     print("对不起，账号或密码输入有误。")
#02
# 编程实现：从键盘输入学生的成绩，输出对应的成绩等级。成绩范围：0 ~ 100分。
# 0<=成绩<60：不及格
# 60<=成绩<70：通过
# 70<=成绩<80：良好
# 80<=成绩<=100：优秀
# 超过100或小于0：输入错误
# credit=int(input("请输入成绩"))
# if 0<=credit<60:
#     print("不及格")
# elif 60<=credit<70:
#     print("通过")
# elif 70<=credit<80:
#     print("良好")
# elif 80<=credit<=100:
#     print("优秀")
# else:
#     print("输入错误")
#03
# 编程实现：输入 0~6 的整数，让其对应星期几。其中0对应星期日，1对应星期一，2对应星期二，3对应星期三，4对应星期四，5对应星期五，6对应星期六，其他数字对应输入错误。输入 0~6 之间的数字，输出星期日, 星期一, 星期二, 星期三, 星期四, 星期五,星期六。
# a={0:"星期日",1:"星期一",2:"星期二",3:
#    "星期三",4:"星期四",5:"星期五",6:"星期六"}
# b=int(input("请输入数字"))
# if 0<=b<=6:
#     print(a[b])
# else:
#     print("输入错误")
#04
# 输入一个整数，输出其绝对值。
# num=int(input("输入一个整数"))
# if num>=0:
#     print(num)
# else:
#     print(-num)
#05
# 定义一个关于时间操作的类TimeTest，其中有一个获得当前时间的函数（静态方法）showTime，在下列代码中请补全这个静态方法。
# 在主程序中，预置了相关的测试代码，运行代码，输出示例所示内容。
import time
# class TimeTest(object):
#     def __init__(self, hour, minute, second):
#         self.hour = hour
#         self.minute = minute
#         self.second = second
#     # 在这里添加静态方法，以时：分：秒的形式显示时间，例如：18:40:53
#     def showTime(self):#定义方法第一个参数是self
#         print("%d:%d:%d"%(self.hour,self.minute,self.second))
# print(TimeTest.showTime())   # 通过类名调用静态方法 showTime()
# t = TimeTest(2,10,10)
# nowTime = t.showTime()      # 通过对象名调用静态方法 showTime()
# print(nowTime)


#06
# class Student:
#     def __init__(self,name):
#         self.num=0
#         self.name=name
#     def addNum(self):
#         self.num+=1
# class Banji(Student):
#
#     def getNum(self,student):
#         print(student.num)
#
# s1 = Student('小王')
# s1.addNum()
# print(Banji.getNum(s1))
# a=[1,2,3]
# b=[1,2,3]
# print(a is b)
# a=10
# b=10
# print(a is b)
# a=(1,2,3)
# b=(1,2,3)
# print(a is b)
# a={1,2,3}
# b={1,2,3}
# print(a is b)
# a=[1,2,3]
# b=[1,2]
# b.append(3)
# print(a is b)



# s=[1,2,3,4]
# s.append({1:2,2:3})
#
# print(len(s))
# Letter = ['a','b','c','v']
# print(Letter[1:2:1])
# print(.join('ABC','DEF'))
# a='世界那么大,我想去看看'
# print(a[7:-3])
# a="Oh!It sounds terrible."
# print(a)
# n=0
# for i in range(1,10,3):
#     n=n+1
# print(n)
# 1,4,7
# 1,2,3
# a="wojsasskjsakkdsasdsad"
# b=a.count('wo')
# print(b)
# TempStr = "Hello World"
# a=len(TempStr)
# print(a)

# ls = [1,2,3,4,5,6,7,8,9]
# lt=ls.append([1,2,3])
# print(ls is lt)
# c='a'+1
# print(c)
list1 = ['Google', 'Runoob', 'Taobao', 'Baidu']
list2 = list1.copy()
print ("list2 列表: ", list2)
list1[1]="haha"
print(list1)
print(list2)

