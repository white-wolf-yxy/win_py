
class Tool(object):
    #类属性会记录跟类相关的特征
    count=0
    @classmethod
    def showCount(cls):
        print(cls.count)
    def __init__(self,name):
        self.name=name
        Tool.count+=1
tool1=Tool("a")
tool1.showCount()#类方法仍是对象调用