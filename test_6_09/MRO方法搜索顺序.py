class A:
    def demo(self):
        print("A我喜欢你")
class B:
    def demo(self):
        print("B我喜欢你")
class C(B,A):
    pass
c=C()
c.demo()
print(C.__mro__)#类搜索方法顺序
# (<class '__main__.C'>, <class '__main__.B'>, <class '__main__.A'>, <class 'object'>)
#object是为所有对象提供的基类

content = input("请输出内容:")
print(f"{content}={eval(content)}")