#定义一个工具类，每个工具都有自己的name，需求：知道自己使用这个类，创建了多少个工具对象
class Tool(object):
    #类属性会记录跟类相关的特征
    count=0
    def __init__(self,name):
        self.name=name
        Tool.count+=1
a=Tool("a")
b=Tool("b")
c=Tool("c")
print(Tool.count)