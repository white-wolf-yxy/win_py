#__new__方法：在内存中为对象分配空间，返回对象的引用
class A:
    def __new__(cls):
        print("创建对象，分配空间")
        instance=super().__new__(cls)#为对象分配空间
        return instance#返回对象的引用
    def __init__(self):
        print("播放器初始化")
player=A()
print(player)