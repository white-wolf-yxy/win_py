#人生重开模拟器
#实现简化版本
#游戏开始的时候，设定初始属性（颜值，体质，智力，家境）
#开始游戏，随机生成行呗和出生点
#针对每一年，都成为人生的经历（依靠一定的随机因素+当前角色的属性）
#打印一个欢迎界面
print("+------------------------------------+")
print("|         花有重开日，人无再少年         |")
print("|         欢迎来到人生重开模拟器         |")
print("+------------------------------------+")
#设置初始属性，颜值，体质，智力，家境（可用点数总数为20，每个在一到十之间）
#通过条件语句对用户的输入进行校验
#使用elif，只能执行一次
#方便用户输错更改程序，采取循环（while True）
while True:
    print("请输入初始属性颜值，体质，智力，家境（可用点数总数为20，每个在一到十之间）")
    face=int(input())
    strong=int(input())
    iq=int(input())
    familycirtuation=int(input())
    if face < 1 and face > 10:
        print('输入错误')
        continue
    if strong < 1 and strong > 10:
        print('输入错误')
        continue
    if iq<1 and iq>10:
        print('输入错误')
        continue
    if familycirtuation<1 and familycirtuation>10:
        print('输入错误')
        continue
    if strong+iq+familycirtuation+face>20:
        print('输入错误，请看好规定')
        continue

    print("属性输入完毕")
    print(f'颜值；{face},体质：{strong},智力：{iq}，家境：{familycirtuation}')
    break