#打印1到10
# for i in range(1,11):
#     print(i)
#打印2，4，6，8，10
#range有三个参数，其中第三个参数控制步长
# for j in range(2,12,2):
#     print(j)
#倒着打印
# num=10
# while num>=1:
#     print(num)
#     num-=1
# for i in range(10,0,-2):
#     print(i)
#打印前100的和
# theSum=0
# for i in range(1,101):
#     theSum+=i
# print(f"theSum={theSum}")
#假设吃5个包子
for i in range(1,6):
    if i==3:
        break
    print(f"吃第{i}个包子")
#还是吃5个包子
for j in range(1,6):
    if j==3:
        continue
    print(f"吃第{j}个包子")
#给定若干个数字，求其平均值，但不知道数字个数
theSum=0
count=0
while True:
    num=input("请输入一个数（输入；代表结束）")
    if num==";":
        break
    theSum+=float(num)
    count+=1
print(f"average={theSum/count}")
