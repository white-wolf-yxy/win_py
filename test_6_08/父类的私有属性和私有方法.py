class Dog:
    def eat(self):
        print('吃')
    def drink(self):
        print('drink')
    def bark(self):
        __age=19
        print(__age)
class XiaoTianQuan(Dog):
    def fly(self):
        print('会飞')
xixi=XiaoTianQuan()
xixi.bark()
#私有属性和方法不能通过外界进行访问，继承无法直接访问父类的私有属性和方法（在对象内部也不行）
#而私有属性可以通过非私有方法访问