#is用于判断两个变量应用对象是否为同一个
#==用于判断应用变量的值是否相等
a=[1,2,3]
print(id(a))
b=[1,2,3]
print(id(b))
print(a is b)


a=[1,2,3]
print(id(a))
b=[1,2]
print(id(b))
b.append(3)
print(id(b))
print(a is b)

#在python中针对None进行比较时，用is比较合适
