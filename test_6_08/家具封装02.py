class HouseItem:
    def __init__(self,name,area):
        self.name=name
        self.area=area
    def __str__(self):
        return("[%s]的面积是[%.2f]"%(self.name,self.area))
bed=HouseItem("bed",4)
chest=HouseItem("chest",2)
table=HouseItem("table",1.5)

class House:
    def __init__(self,Harea,Htype):
        self.Harea=Harea
        self.Htype=Htype
        self.Hitems=[]
        self.Hrarea=Harea
    def add_item(self,item):
        self.Hitems.append(item.name)
        self.Hrarea-=item.area
    def __str__(self):
        return"户型是[%s],总面积是[%.2f],剩余面积是[%.2f]"%(self.Htype,self.Harea,self.Hrarea)
big=House(100,'大别墅')
big.add_item(bed)
big.add_item(chest)
big.add_item(table)
print(big)
print(f'家具名称列表是{big.Hitems}')
#想用其他类中的方法，就在外部增加一个属性
#而想用其它类中的的对象，可以通过传参实现
#对象调用方法和属性

