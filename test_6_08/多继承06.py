# 子类可以有多个父亲，并且具有所有父亲的属性和方法
class A:
    def test(self):
        print("test方法")
class B:
    def demo(self):
        print("demo方法")
class C(A,B):
    pass
x=C()
x.test()
x.demo()

#如果不同的父亲中存在同名的方法，应该尽量避免能使用多继承
