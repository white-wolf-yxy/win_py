# 面向对象的特性：封装继承和多态
#继承是实现代码的重用，相同的代码不需要重复的编写
#单继承
class Animal:
    def eat(self):
        print('吃...')
    def drink(self):
        print('喝...')
    def run(self):
        print('跑...')


class Dog(Animal):
    # def eat(self):
    #     print('吃')
    # def drink(self):
    #     print('喝')
    # def run(self):
    #     print('跑')
    def bark(self):
        print('汪汪叫')
#继承的概念：子类拥有父类的所有方法和属性
# wangcai=Dog()
# wangcai.eat()
# wangcai.drink()
# wangcai.run()
# wangcai.bark()
#基类就是父类，派生类就是子类，继承就是派生
#继承的传递性
class Xiaotianquan(Dog):
    def fly(self):
        print('我会飞')
wangcai=Xiaotianquan()
wangcai.eat()
wangcai.drink()
wangcai.run()
wangcai.bark()
wangcai.fly()
#子类拥有父亲以及父亲的父亲中封装的户型和方法
#方法的重写：覆盖父类的方法
#对父类方法的扩写