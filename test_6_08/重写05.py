#覆盖父类的方法
class Dog:
    def eat(self):
        print('吃')
    def drink(self):
        print('drink')
    def bark(self):
        print('汪汪叫')



class XiaoTianQuan(Dog):
    def bark(self):
        print('像神一样叫')
        super().bark()#super()就是super类创建出来的变量，调用父类的方法
        print("$%$%$%$%$%$%$")
xixi=XiaoTianQuan()
xixi.bark()