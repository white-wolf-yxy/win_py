# 一个对象的属性也可以是另一个类创建的对象
#士兵突击
class Gun:
    def __init__(self,model):
        self.model=model
        self.count=0
    def add_count(self,count):
        self.count+=count
    def shoot(self):
        #判断子弹个数，准备发射条件
        if self.count<=0:
            print("[%s]没有子弹了"%(self.model))
        else:
            #发射一次就减掉一颗子弹
            self.count-=1
            print("[%s]还剩[%d],突突突"%(self.model,self.count))
#创建对象ak47
ak47=Gun("ak47")

#属性名和方法名不能一致


#在定义属性时，如果不知道设置什么初始值，可以设置为None
class Sodior:
    def __init__(self,name):
        self.name=name
        self.gun=None
    def fire(self):
        if self.gun==None:
            print("[%s]没抢"%(self.name))
            return
        print('冲冲冲..')
        self.gun.add_count(30)
        self.gun.shoot()

xusanduo=Sodior("许三多")
#在外部增加属性
xusanduo.gun=ak47#通过对象调用属性和方法
print(xusanduo.gun)
xusanduo.fire()

