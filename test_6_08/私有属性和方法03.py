#形式：就是在属性名和方法名前加__
class Women:
    def __init__(self,name):
        self.name=name
        self.__age=18
    def secret(self):
        print('[%s]的年龄是[%d]'%(self.name,self.__age))
xiaofang=Women('xiaofang')
xiaofang.secret()
#私有属性，方法在外界不能够被直接访问
#在对象的方法啊内部，是可以访问私有属性的