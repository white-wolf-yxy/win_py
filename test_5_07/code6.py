#变量的作用域
# def getpoint():
#     x=10
#     y=10
#     return x,y
# getpoint()
# print(x,y)
#超出作用域
# def getpoint():
#     x=10
#     y=10
#     return x,y
# x,y=getpoint()
# print(x,y)
# x=10#全局变量
# def point():
#     x=20#局部变量
#     print(f'x为{x}')
# point()
# print(f'x为{x}')
#在函数里面尝试访问全局变量（在函数中访问某个变量的时候，会现场时在局部变量中查找，找不到就去上一级作用域中找）
#gLobol全局申明
x=10
def oi():
    global x
    x=20
oi()
print(f'x为{x}')
#然而for，else，if语句并不会对作用域产生影响
for i in range(1,11):
    print(i)
print('---------------------')
print(i)