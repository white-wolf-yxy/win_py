#这种情况不算多个return打印
# def test():
#     return 1
#     return 2
# the=test()
# print(the)
#一般多个return语句需要搭配分支语句或者循环语句
def isodd(num):
    if num % 2 == 0:
        return '偶数'
    else:
        return '奇数'
num=int(input())
type=isodd(num)
print(type)