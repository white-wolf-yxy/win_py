#使用函数的方式来就解决求和操作
def calcSum(beg,end):
    theSum=0
    for i in range(beg,end+1):
        theSum+=i
    print(f'{beg}-{end}的和为{theSum}')
#调用函数
#求1-100的和
calcSum(1,100)
#求300-400的和
calcSum(300,400)
#求1-1000的和
calcSum(1,1000)