#创建元组
a=()
print(type(a))
b=tuple()
print(type(b))
#创建元组的时候，指定初始值
a=(1,2,3,4,5)
print(a)
#元组中的元素也可以是任意类型
a=(1,2,'hello',True,[])
print(a)
#通过下标访问元组
a=(1,2,3,4,5)
print(a[1])
# print(a[100])
#通过切片来获取元组中的一个部分
print(a[1::2])
#使用for循环来遍历元素
a=(1,2,3,4,5)
for i in a:
    print(i)
for i in range(0,len(a)):
    print(a[i])
#使用in来判断元素是否在元组中，使用index查找元素的下标
a=(1,2,3,4,5,6)
print(1 in a)
b=a.index(3)
print(b)
#可以使用+来拼接两个元组
a=(1,2,3,4,5)
b=(6,7,8,9,10)
print(a+b)
# a+=b
# print(a)
# a.expend(b)
# print(a)
#元组只支持读的操作，不支持修改的操作
# a=(1,2,3,4,5)
# a[2]=90
# print(a)#错误操作
